# This is calendar

A platform to organize your events.

## For users:

The project is made to manage daily events. 

Current features are:
    - Login
    - Events (Description, time to start and time to end)

## For developers:

You will need docker, node and go packages to run the project.



After clone the repository, please, follow these steps:

    - back stuff

        1. Change directory to api;
        2. Run 'make postgress' in your terminal to create your local database;
        3. Create your api go binary using 'go build api';
        4. Run 'go run api' to execute the server;
    
    - front stuff

        1. Change directory to ui;
        2. Run 'npm install' to install the project's dependencies;
        3. Add your secret API key for authentication, from firebase, in file src/constants.js; 
        4. Use the command 'npm start dev' to run the application;

Now you are ready to manage your events!

## Technology

Calendar uses  

- [Echo](https://echo.labstack.com/)
- [Cobra](https://github.com/spf13/cobra)
- [sqlx](https://github.com/jmoiron/sqlx)
- [pq](https://github.com/lib/pq)
- [validator](https://godoc.org/gopkg.in/go-playground/validator.v9)
- [Postgres](https://www.postgresql.org/)
- [Firebase Authentication](https://firebase.google.com/docs/auth)
- [React](https://pt-br.reactjs.org/)
- [Ant Design](https://ant.design/)

Any problems?
    Oops, my bad. Feel free to send a message to me.

Comming soon
    - Test for ui with Jest and Enzyme
    - Login and SignUp layout refactor
    - Test for api with Testify and Go-sqlmock

Made with love by N-th.
