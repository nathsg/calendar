module.exports = {
	devtool: "source-map",
	entry: "./src/index.js",
	module: {
		rules: [
		{
			test: /\.(js|jsx)$/,
			exclude: /node_modules/,
			use: ["babel-loader"]
		},
		{
			test: /\.css$/,
			use: ["style-loader", "css-loader", "less-loader"]
		},
		{
			test: /\.less$/,
			use: ["style-loader", "css-loader", "less-loader"]
		}
		]
	},
	resolve: {
		extensions: [".js", ".jsx"]
	},
	output: {
		filename: "bundle.js"
	},
	devServer: {
		contentBase: "./dist",
		compress: true,
		port: 8080
	}
}
