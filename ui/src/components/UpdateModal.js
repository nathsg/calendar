import React from "react"
import axios from "axios"

import { Form, Input, Button, Modal, DatePicker, message } from "antd"

const UpdateModal = ({ open, fetchEvents, handleClose, onFinishFailed, eventData }) => {
    const onFinish = async values => {
        await (
            console.log(values, eventData)
        )
        const response = {}
        console.log(values, eventData)
        response["event_id"] = eventData.event_id
        response["description"] = eventData.description
        response["start_at"] = values.start_at.format("HH:mm")
        response["finish_at"] = values.finish_at.format("HH:mm")
        response["start_day"] = values.start_at.format("DD-MM-YYYY")
        response["finish_day"] = values.finish_at.format("DD-MM-YYYY")
        console.log("response", response)

        axios.put("/api/event", response)
        .then(function () {
            message.success("Updated event")
            handleClose()
            fetchEvents()
        })
        .catch(function (error) {
            console.log(error)
            message.error("Oops, something went wrong when updating event")
        })
    }

    return(
        <Modal
          title="Update an event"
          visible={open}
          onCancel={handleClose}
          footer={null}
          destroyOnClose={true}
        >
           <Form
                name="Updating event"
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                    label="Description"
                    name="description"
                >
                    <Input defaultValue={eventData.description} disabled/>
                </Form.Item>
                <Form.Item layout="inline">
                    <Form.Item name="start_at" label="Start time"
                        style={{ display: "inline-block", width: "calc(50% - 5px)" }}
                        rules={[{ required: true, message: "Please input your event's start time!" }]}
                    >
                        <DatePicker
                            showTime
                            format="YYYY-MM-DD HH:mm"
                        />
                    </Form.Item>
                    <Form.Item name="finish_at" label="End time"
                        style={{ display: "inline-block", width: "calc(50% - 5px)" }}
                        rules={[{ required: true, message: "Please input your event's end time!" }]}
                    >
                        <DatePicker
                            showTime
                            format="YYYY-MM-DD HH:mm"
                        />
                    </Form.Item>
                </Form.Item>
                <Form.Item style={{textAlign: "right"}}>
                    <Button 
                        type="primary" 
                        htmlType="submit"
                    >
                        Submit
                    </Button>
                    <Button 
                        onClick={handleClose}
                        style={{marginLeft: "10px"}}
                    >
                        Cancel
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default UpdateModal