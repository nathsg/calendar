import React from "react"
import axios from "axios"

import { Form, Input, Button, Modal, DatePicker, message } from "antd"

const AddModal = ({ user, open, handleClose, fetchEvents, onFinishFailed }) => {
    const onFinish = values => {
        const response = {}
        response["user_id"] = user
        response["description"] = values.description
        response["start_at"] = values.start_at.format("HH:mm")
        response["finish_at"] = values.finish_at.format("HH:mm")
        response["start_day"] = values.start_at.format("DD-MM-YYYY")
        response["finish_day"] = values.finish_at.format("DD-MM-YYYY")

        axios.post("/api/event", response)
        .then(function () {
            handleClose()
            fetchEvents()
            message.success("Added event")
        })
        .catch(function (error) {
            console.log(error)
            message.error("Oops,something went wrong when adding event")
        })
    }


    return(
        <Modal
          title="Add an event"
          visible={open}
          onCancel={handleClose}
          footer={null}
          destroyOnClose={true}
        >
           <Form
                name="Adding event"
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                >
                <Form.Item
                    label="Description"
                    name="description"
                    rules={[{ required: true, message: "Please input your event's description!" }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item layout="inline">
                    <Form.Item name="start_at" label="Start time"
                        style={{ display: "inline-block", width: "calc(50% - 5px)" }}
                        rules={[{ required: true, message: "Please input your event's start time!" }]}
                    >
                        <DatePicker
                            showTime
                            format="YYYY-MM-DD HH:mm"
                        />
                    </Form.Item>
                    <Form.Item name="finish_at" label="End time"
                        style={{ display: "inline-block", width: "calc(50% - 5px)" }}
                        rules={[{ required: true, message: "Please input your event's end time!" }]}
                    >
                        <DatePicker
                            showTime
                            format="YYYY-MM-DD HH:mm"
                        />
                    </Form.Item>
                </Form.Item>
                <Form.Item style={{textAlign: "right"}}>
                    <Button 
                        type="primary" 
                        htmlType="submit"
                    >
                        Submit
                    </Button>
                    <Button 
                        onClick={handleClose}
                        style={{marginLeft: "10px"}}
                    >
                        Cancel
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default AddModal