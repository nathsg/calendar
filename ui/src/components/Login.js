import React, { useCallback, useContext } from "react"
import { withRouter, Redirect } from "react-router"
import { Link } from "react-router-dom"

import app from "./base.js"
import { AuthContext } from "./Auth.js"

import { Button, Input } from "antd"

const Login = ({ history }) => {
	const handleLogin = useCallback(async event => {
		event.preventDefault()
		const { email, password } = event.target.elements
		try {
			await app
			.auth()
			.signInWithEmailAndPassword(email.value, password.value)
			history.push("/")
		} catch (error) {
			alert(error)
		}
	}, [history])

	const { currentUser } = useContext(AuthContext)

	if (currentUser) {
		return <Redirect to="/" />
	}

	return (
		<div style={{backgroundColor: "#E6F7FE", padding: "50px", height: "100vh"}}>
			<h1 style={{fontSize: "50px", color: "#1C8FFB"}}>Log in</h1>
			<form onSubmit={handleLogin}>
				<label>
					Email
					<Input name="email" type="email" placeholder="Email" />
				</label>
				<label style={{marginBottom: '50px'}}>
					Password
					<Input name="password" type="password" placeholder="Password" />
				</label>
				<Button type="submit" htmlType="submit">Log in</Button>
				<Button>
					<Link to="/signup">Sign up</Link>
				</Button>
			</form>
		</div>
	)
}

export default withRouter(Login)


// import React, { useCallback, useContext } from "react"
// import { withRouter, Redirect } from "react-router"
// import { Link } from "react-router-dom"
// import app from "./base.js"
// import { AuthContext } from "./Auth.js"

// import { Button, Input, Form } from 'antd'

// const Login = ({ history }) => {
// 	const handleLogin = useCallback(
// 		async event => {
// 			event.preventDefault()
// 			const { email, password } = event.target.elements
// 			try {
// 				await app
// 				.auth()
// 				.signInWithEmailAndPassword(email.value, password.value)
// 				history.push("/")
// 			} catch (error) {
// 				alert(error)
// 			}
// 		}, [history])
	
// 	const onFinish = (values) => {useCallback(
// 		async event => {
// 			const { email, password } = values
// 			try {
// 				await app
// 				.auth()
// 				.signInWithEmailAndPassword(email.value, password.value)
// 				history.push("/")
// 			} catch (error) {
// 				alert(error)
// 			}
// 		}, [history])}

// 	const onFinishFailed = errorInfo => {
//         message.error('Oops,something went wrong')
//     }

// 	const { currentUser } = useContext(AuthContext)

// 	if (currentUser) {
// 		return <Redirect to="/" />
// 	}

// 	return (
// 		<div>
// 			<h1>Log in</h1>
// 			<Form 
// 				name="login"
// 				onFinish={useCallback(
// 					async event => {
// 						try {
// 							console.log(event)
// 							const { email, password } = event

// 							await app
// 							.auth()
// 							.signInWithEmailAndPassword(email, password)
// 							history.push("/")
// 						} catch (error) {
// 							alert(error)
// 						}
// 					}, [history])}
//                 onFinishFailed={onFinishFailed}
// 				>
// 				<Form.Item
// 					label="email"
// 					name="email"
// 				>
// 					<Input />
// 				</Form.Item>
// 				<Form.Item
// 					label="password"
// 					name="password"
// 				>
// 					<Input.Password />
// 				</Form.Item>
// 				<Form.Item>
// 					<Button 
// 						type="submit"
//                         htmlType="submit"
// 					>
// 						Log in
// 					</Button>
// 					<Button>
// 						<Link to="/signup">Sign up</Link>
// 					</Button>
// 				</Form.Item>
// 			</Form>
// 		</div>
// 	)
// }

// export default withRouter(Login)