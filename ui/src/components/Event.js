import React, {useEffect, useState, useContext } from "react"
import axios from "axios"

import app from "./base"
import { AuthContext } from "./Auth.js"

import { Calendar, Button, message } from "antd"

import AddModal from "./AddModal"
import UpdateModal from "./UpdateModal"
import Date from "./Date"

var moment = require("moment")

const Event = () => {
    const { currentUser } = useContext(AuthContext)

    const [time, setTime] = useState({date: moment().format("DD-MM-YYYY"), weekDay: moment().format("ddd"),day: moment().format("DD"), month: moment().format("MMM"), year: moment().format("YYYY")})
    const [data, setData] = useState({events: [], isFetching: false})
    const [user, setUser] = useState("")
    const [open, setOpen] = useState(false)
    const [openUpdateModal, setOpenUpdateModal] = useState(false)
    const [eventData, setEventData] = useState({})

    const onSelect = value => {
        setTime({date: value.format("DD-MM-YYYY"), weekDay: value.format("ddd"), day: value.format("DD"), month: value.format("MMM"), year: value.format("YYYY")})
    }

    const fetchEvents = async () => {
        try {
            setData({events: data.events, isFetching: true})
            const responseUser = await axios.get(`/api/users/${currentUser.email}`)
            const user = responseUser.data.user_id
            setUser(user)

            const responseEvents = await axios.get(`/api/${user}/events/${time.date}`)
            setData({events: responseEvents.data, isFetching: false})
        } catch (error) {
            console.log(error)
            setData({events: data.events, isFetching: false})
        }
    }

    useEffect(() => {
        fetchEvents()
    }, [time.date], user)

    const handleOpen = () =>{
        setOpen(true)
    }

    const handleClose = () => {
        setOpen(false)
        setOpenUpdateModal(false)
    }
            
    const onFinishFailed = errorInfo => {
        message.error("Oops, something went wrong while in modal")
    }

    const editEvent = event => {
        setEventData(event)
        setOpenUpdateModal(true)
    }

    return(
        <div style={{display: "flex"}}>
            <Button style={{position: "absolute", top: "14px", right: "2%"}} onClick={() => app.auth().signOut()}>Sign out</Button>
            <div style={{width: "50%", margin: "30px"}}>
                <Calendar fullScreen={false} onSelect={onSelect}/>
            </div>
            <Date time={time} editEvent={editEvent} fetchEvents={fetchEvents} handleOpen={handleOpen} data={data}/>
            <AddModal user={user} open={open} fetchEvents={fetchEvents} handleClose={handleClose} onFinishFailed={onFinishFailed} />
            <UpdateModal open={openUpdateModal} fetchEvents={fetchEvents} handleClose={handleClose} onFinishFailed={onFinishFailed} eventData={eventData}/>
        </div>
    )
}

export default Event