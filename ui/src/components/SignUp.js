import React, { useCallback } from "react"
import { withRouter } from "react-router"
import axios from "axios"

import app from "./base"

import { Button, Input, message } from 'antd'

const SignUp = ({ history }) => {
	const handleSignUp = useCallback(async event => {
		event.preventDefault()
		const { email, password } = event.target.elements
		try {
			await app
			.auth()
			.createUserWithEmailAndPassword(email.value, password.value)
			history.push("/")

			axios.post("/api/user", {'email': email.value})
			.then(function () {
				message.success("User added")
			})
			.catch(function (error) {
				message.error("Oops,something went wrong when adding user")
			})
		} catch (error) {
			alert(error)
		}
	}, [history])

	return (
		<div style={{backgroundColor: "#E6F7FE", padding: "50px", height: "100vh"}}>
		<h1 style={{fontSize: "50px", color: "#1C8FFB"}}>Sign up</h1>
			<form onSubmit={handleSignUp}>
				<label>
				Email
				<Input name="email" type="email" placeholder="Email" />
				</label>
				<label>
				Password
				<Input name="password" type="password" placeholder="Password" />
				</label>
				<Button type="submit" htmlType="submit">Sign Up</Button>
			</form>
		</div>
	)
}

export default withRouter(SignUp)