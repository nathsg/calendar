import React from "react"
import axios from "axios"

import { List, Button, Empty, Popconfirm, message } from "antd"
import { PlusOutlined, DeleteOutlined, EditOutlined } from "@ant-design/icons"

const Date = ({ time, editEvent, handleOpen, data, fetchEvents }) => {
    const confirm = e =>{
        deleteEvent(e.event_id)
    }

    const deleteEvent = async eventID => {
        try {
            await axios.delete(`/api/event/${eventID}`)
            message.success("Event deleted")
            fetchEvents()
        } catch (error) {
            console.log(error)
            message.error("Oops, something went wrong when deleting event")
        }
    }

    return(
        <div style={{ width: "45%"}}>
            <div style={{fontSize: "50px", margin: "50px 50px 0", color: "#1C8FFB"}}>
                {time.weekDay} {time.day}, {time.month}
            </div>
            <div style={{overflowY: "auto", height: "60vh", marginRight: "10px", borderLeft: "1px solid #E6F7FE"}}>
                {data.events ?
                    <List 
                        style={{margin: "20px 10px 20px 50px", maxHeigh: "100px"}}
                        bordered
                        dataSource={data.events}
                        renderItem={
                            item=> (
                                <List.Item key={item.eventID}>
                                    <div>
                                        {item.start_at} - {item.finish_at}
                                    </div>
                                    <div style={{fontSize: "18px", textOverflow: "ellipsis", overflow: "hidden", whiteSpace: "nowrap", width: "50%"}}>
                                        {item.description}
                                    </div> 
                                    <div>
                                        <Button icon={<EditOutlined />} style={{marginRight: "5px"}} onClick={() => editEvent(item)}/>
                                        <Popconfirm
                                            title="Are you sure delete this event?"
                                            onConfirm={() => confirm(item)}
                                            okText="Yes"
                                            cancelText="No"
                                        >
                                            <Button icon={<DeleteOutlined />} />
                                        </Popconfirm>
                                    </div>
                                </List.Item>
                            )
                        }
                    /> : <Empty 
                            image={Empty.PRESENTED_IMAGE_SIMPLE} 
                            style={{marginTop: '200px'}}
                            description={
                              <span style={{color: "#1C8FFB"}}>
                                No events
                              </span>
                            }
                        />
                }
            </div>
            <Button 
                shape="circle" 
                type="primary"
                size="large" 
                icon={<PlusOutlined />} 
                onClick={handleOpen}
                style={{position: "fixed", bottom: "5%", right: "5%"}}
            />
        </div>
    )
}

export default Date