import { firebase } from "@firebase/app"
import "firebase/auth"

import { APIKEY } from "../constants"

const app = firebase.initializeApp({
    apiKey: APIKEY,
})

export default app