import React from "react"
import { BrowserRouter as Router, Route} from "react-router-dom"

import Event from "./components/Event"
import {AuthProvider} from "./components/Auth"
import PrivateRoute from "./components/PrivateRoute"
import SignUp from "./components/SignUp"
import Login from "./components/Login"


const App = () => {
    return(
        <AuthProvider>
            <Router>
                <PrivateRoute exact path="/" component={Event} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/signup" component={SignUp} />
            </Router>
        </AuthProvider>
    )
}

export default App