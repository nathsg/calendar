FROM golang:latest AS API

WORKDIR /app/api
ADD api /app/api
RUN make dev


FROM node:14.5.0-alpine3.11 AS UI

WORKDIR /app/ui
ADD ui/package.json /app/ui/package.json
RUN npm install

ADD ui /app/ui
RUN npm run build


FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=API ./app/api ./api
COPY --from=UI ./app/ui/dist ./ui/dist
RUN chmod +x ./api/main
EXPOSE 5555
RUN cd api && ls
CMD ./api/main
