package main

import (
	"github.com/golang/glog"
	"gitlab.com/felipemocruha/gonfig"
)

func main() {
	config := &Config{}
	if err := gonfig.LoadFromEnv("CONFIG_PATH", config, "./api/config/config.yaml"); err != nil {
	// if err := gonfig.LoadFromEnv("CONFIG_PATH", config, "./config/config.yaml"); err != nil {
			glog.Fatalf("Failed to load config: %v", err)
	}

	service, err := NewService(config)
	if err != nil {
		glog.Fatalf("failed to create main service: %v", err)
	}

	server, err := service.createServer()
	if err != nil {
		glog.Fatalf("Failed to create the server: %v", err)
	}

	apiServerListen(server)
}
