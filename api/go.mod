module api

go 1.14

require (
	github.com/go-playground/validator/v10 v10.2.0
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/jmoiron/sqlx v1.2.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lib/pq v1.3.0
	gitlab.com/felipemocruha/gonfig v0.1.0
	golang.org/x/crypto v0.0.0-20200302210943-78000ba7a073 // indirect
)
