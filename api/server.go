package main

import (
	"log"

	"github.com/go-playground/validator/v10"
	"github.com/golang/glog"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
)

const (
	METRICS_ADDR = "0.0.0.0:8888"
	HOST         = "0.0.0.0:5555"
)

func apiServerListen(server *echo.Echo) {
	log.Printf("starting api server at: %v", HOST)
	log.Fatal(server.Start(HOST))
}

func createDB(config *Config) *sqlx.DB {

	db, err := sqlx.Connect("postgres", makeConnStr(config))
	if err != nil {
		glog.Fatalf("failed to create database connection: %v", err)
	}

	db.MustExec(schema)

	return db
}

func (s *Service) createServer() (*echo.Echo, error) {
	server := echo.New()
	server.Validator = &CustomValidator{validator: validator.New()}

	api := server.Group("/api")

	api.POST("/user", s.AddUser)
	api.GET("/users", s.ListUsers)
	api.GET("/users/:user_email", s.GetUser)

	api.POST("/event", s.AddEvent)
	api.GET("/:user_id/events", s.ListEvents)
	api.GET("/:user_id/events/:date", s.ListDateEvents)
	api.DELETE("/:user_id/event/:event_id", s.DeleteEvent)
	api.PUT("/event", s.UpdateEvent)

	server.GET("/healthcheck", func(ctx echo.Context) error {
		return nil
	})

	server.File("/", "../api/static/index.html")
	server.File("/bundle.js", "../ui/dist/bundle.js")

	return server, nil
}

func NewService(config *Config) (*Service, error) {
	svc := &Service{
		Store:  &StoreImpl{DB: createDB(config)},
		Config: config,
	}

	return svc, nil
}
