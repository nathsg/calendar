package main

import (
	"fmt"

	_ "github.com/lib/pq"
)

var schema = `
	CREATE TABLE IF NOT EXISTS client (
		user_id SERIAL NOT NULL PRIMARY KEY,
		email text UNIQUE NOT NULL
	);
	CREATE TABLE IF NOT EXISTS event (
		event_id SERIAL NOT NULL PRIMARY KEY,
		user_id SERIAL NOT NULL,
		description text NOT NULL,
		start_at text NOT NULL,
		start_day text NOT NULL,
		finish_at text NOT NULL,
		finish_day text NOT NULL,

		CONSTRAINT FK_client_user_id FOREIGN KEY (user_id) REFERENCES client (user_id)
	);
`

func makeConnStr(config *Config) string {
	return fmt.Sprintf(
		"host=%s user=%s dbname=%s sslmode=disable password=%s",
		config.DB.Host,
		config.DB.User,
		config.DB.Name,
		config.DB.Password,
	)
}
