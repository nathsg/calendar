package main

import (
	"github.com/go-playground/validator/v10"
	"github.com/jmoiron/sqlx"
)

const (
	AddUserStringResponse       = "user sucessfuly added"
	ListEmailUserStringResponse = "list user for email %v"

	AddEventStringResponse      = "event sucessfuly added"
	DeleteEventStringResponse   = "event %v sucessfuly deleted"
	UpdateEventStringResponse   = "event sucessfuly updated"
	ListDateEventStringResponse = "list events in date %v"
)

type User struct {
	UserID int    `json:"user_id" db:"user_id"`
	Email  string `json:"email" validate:"required" db:"email"`
}

type Event struct {
	EventID     int    `json:"event_id" db:"event_id"`
	UserID      int    `json:"user_id" validate:"required" db:"user_id"`
	Description string `json:"description" validate:"required" db:"description"`
	StartAt     string `json:"start_at" validate:"required" db:"start_at"`
	StartDay    string `json:"start_day" validate:"required" db:"start_day"`
	FinishAt    string `json:"finish_at" validate:"required" db:"finish_at"`
	FinishDay   string `json:"finish_day" validate:"required" db:"finish_day"`
}

type dbConfig struct {
	Host     string `yaml:"host"`
	User     string `yaml:"user"`
	Name     string `yaml:"name"`
	Password string `yaml:"password"`
}

type StoreImpl struct {
	DB *sqlx.DB
}

type Config struct {
	Debug bool
	DB    dbConfig `yaml:"db"`
}

type Service struct {
	Store  Store
	Config *Config
}

type Store interface {
	AddUser(request User) error
	ListUsers() ([]User, error)
	GetUser(email string) (*User, error)

	AddEvent(request Event) error
	ListEvents(userID string) ([]Event, error)
	ListDateEvents(userID string, date string) ([]Event, error)
	DeleteEvent(EventID string) error
	UpdateEvent(request Event) error
}

type CustomValidator struct {
	validator *validator.Validate
}
