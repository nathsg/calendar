package main

const AddEvent = `
INSERT INTO event (user_id, description, start_at, start_day, finish_at, finish_day)

VALUES (:user_id, :description, :start_at, :start_day, :finish_at, :finish_day);
`

const AddUser = `
INSERT INTO client (email)

VALUES (:email)
`

const ListUsers = `
SELECT user_id, email FROM client ORDER BY email ASC 
`

const GetUser = `
SELECT user_id, email FROM client WHERE email = $1 LIMIT 1
`

const ListEvents = `
SELECT user_id, event_id, description, start_at, start_day, finish_at, finish_day FROM event WHERE user_id = $1 ORDER BY description ASC 
`

const ListDateEvents = `
SELECT event_id, description, start_at, start_day, finish_at, finish_day  FROM event WHERE (user_id= $1 AND start_day = $2)`

const DeleteEventByID = `DELETE FROM event WHERE event_id = $1`

const UpdateEventByID = `
UPDATE event

SET description = $3, start_at = $4, start_day = $5, finish_at = $6, finish_day = $7

WHERE event_id = $1`
