package main

import "fmt"

func (s StoreImpl) AddUser(request User) (err error) {
	if _, err := s.DB.NamedExec(AddUser, request); err != nil {
		return err
	}

	return nil
}

func (s StoreImpl) ListUsers() (user []User, err error) {
	if err := s.DB.Select(&user, ListUsers); err != nil {
		return nil, err
	}

	return user, nil
}

func (s StoreImpl) GetUser(userEmail string) (*User, error) {
	var user User

	err := s.DB.Get(&user, GetUser, userEmail)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (s StoreImpl) AddEvent(request Event) (err error) {
	if _, err := s.DB.NamedExec(AddEvent, request); err != nil {
		return err
	}

	return nil
}

func (s StoreImpl) ListEvents(userID string) (event []Event, err error) {
	if err := s.DB.Select(&event, ListEvents, userID); err != nil {
		return nil, err
	}

	return event, nil
}

func (s StoreImpl) ListDateEvents(userID string, date string) (event []Event, err error) {
	if err := s.DB.Select(&event, ListDateEvents, userID, date); err != nil {
		return nil, err
	}

	return event, nil
}

func (s StoreImpl) DeleteEvent(EventID string) error {
	_, err := s.DB.Exec(DeleteEventByID, EventID)
	return err
}

func (s StoreImpl) UpdateEvent(request Event) (err error) {
	if a, err := s.DB.Exec(UpdateEventByID, request.EventID, request.Description, request.StartAt, request.StartDay, request.FinishAt, request.FinishDay); err != nil {
		fmt.Println(a)
		return err
	}

	return nil
}
