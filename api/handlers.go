package main

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/golang/glog"
	"github.com/labstack/echo"
)

var JSONDecodeError = errors.New("failed to decode json input")
var JSONValidationError = errors.New("failed to validate json input")
var DatabaseError = errors.New("database internal error")
var EmptyResultError = errors.New("sql: no rows in result set")

func (s Service) AddUser(ctx echo.Context) error {
	request := new(User)

	if err := ctx.Bind(request); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, JSONDecodeError)
	}

	if err := ctx.Validate(request); err != nil {
		return echo.NewHTTPError(http.StatusNotAcceptable, JSONValidationError)
	}

	if err := s.Store.AddUser(*request); err != nil {
		glog.Errorf("failed to add user: %v", err)
		return echo.NewHTTPError(http.StatusInternalServerError, DatabaseError)
	}

	return ctx.String(http.StatusOK, AddUserStringResponse)
}

func (s Service) ListUsers(ctx echo.Context) error {
	users, err := s.Store.ListUsers()
	if err != nil {
		glog.Errorf("failed to list users: %v", err)
		return echo.NewHTTPError(http.StatusInternalServerError, DatabaseError)
	}

	return ctx.JSON(http.StatusOK, users)
}

func (s Service) GetUser(ctx echo.Context) error {
	userEmail := ctx.Param("user_email")

	user, err := s.Store.GetUser(userEmail)
	if err != nil {
		glog.Errorf("failed to get user %v: %v", userEmail, err)
		return echo.NewHTTPError(http.StatusInternalServerError, DatabaseError)
	}

	return ctx.JSON(http.StatusOK, user)
}

func (s Service) AddEvent(ctx echo.Context) error {
	request := new(Event)

	if err := ctx.Bind(request); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, JSONDecodeError)
	}

	if err := ctx.Validate(request); err != nil {
		return echo.NewHTTPError(http.StatusNotAcceptable, JSONValidationError)
	}

	if err := s.Store.AddEvent(*request); err != nil {
		glog.Errorf("failed to add event: %v", err)
		return echo.NewHTTPError(http.StatusInternalServerError, DatabaseError)
	}

	return ctx.String(http.StatusOK, AddEventStringResponse)
}

func (s Service) ListEvents(ctx echo.Context) error {
	userID := ctx.Param("user_id")

	events, err := s.Store.ListEvents(userID)
	if err != nil {
		glog.Errorf("failed to list events: %v", err)
		return echo.NewHTTPError(http.StatusInternalServerError, DatabaseError)
	}

	return ctx.JSON(http.StatusOK, events)
}

func (s Service) ListDateEvents(ctx echo.Context) error {
	userID := ctx.Param("user_id")
	date := ctx.Param("date")

	events, err := s.Store.ListDateEvents(userID, date)
	if err != nil {
		glog.Errorf("failed to list events for user %v at date %v: %v", userID, date, err)
		return echo.NewHTTPError(http.StatusInternalServerError, DatabaseError)
	}

	return ctx.JSON(http.StatusOK, events)
}

func (s Service) DeleteEvent(ctx echo.Context) error {
	eventID := ctx.Param("event_id")

	if err := s.Store.DeleteEvent(eventID); err != nil {
		glog.Errorf("failed to delete event with ID %v: %v", eventID, err)
		return echo.NewHTTPError(http.StatusInternalServerError, DatabaseError)
	}

	return ctx.String(http.StatusOK, fmt.Sprintf(DeleteEventStringResponse, eventID))
}

func (s Service) UpdateEvent(ctx echo.Context) error {
	request := new(Event)

	if err := ctx.Bind(request); err != nil {
		fmt.Println(err)
		return echo.NewHTTPError(http.StatusInternalServerError, JSONDecodeError)
	}

	if err := ctx.Validate(request); err != nil {
		return echo.NewHTTPError(http.StatusNotAcceptable, JSONValidationError)
	}

	if err := s.Store.UpdateEvent(*request); err != nil {
		glog.Errorf("failed to update event %v", err)
		return echo.NewHTTPError(http.StatusInternalServerError, DatabaseError)
	}

	return ctx.String(http.StatusOK, UpdateEventStringResponse)
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}
